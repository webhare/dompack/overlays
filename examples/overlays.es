import * as dompack from "dompack";
import OverlayManager from '../src/index.es';
import "./overlays.scss";

/*
place this somewhere in your HTML:

<div class="withoverlays" style="position: relative; width: 500px; height: 500px; background-color: #EFE;">
</div>
*/

dompack.register(".withoverlays", (node, idx) =>
{
  let mybounds = { left: 0, top: 0, right: node.clientWidth-1, bottom: node.clientHeight-1 };
  let overlaymgr = new OverlayManager(node, "myoverlay", { bounds: mybounds, allowcreate: true });

  node.addEventListener('dompack:overlay-selectionchange', event =>
  {
    dompack.qS('#selection').textContent = overlaymgr.getSelection().map(node => node.getContentNode().textContent).join(', ');
  });

  node.addEventListener('dompack:overlay-selectionchange', event =>
  {
    let node = dompack.qS('#selectionchanges' + (event.detail.useraction ? 'user' : ''));
    node.textContent = (parseInt(node.textContent) || 0) + 1;
  });
  node.addEventListener('dompack:overlay-areachange', event =>
  {
    let node = dompack.qS('#areachanges' + (event.detail.useraction ? 'user' : ''));
    node.textContent = (parseInt(node.textContent) || 0) + 1;
  });
  node.addEventListener('dompack:overlay-created', event =>
  {
    let node = dompack.qS('#created');
    node.textContent = (parseInt(node.textContent) || 0) + 1;
  });

  let amiga = overlaymgr.addRectangle({ left: 5, top: 50, width: 50, height: 150 });
  dompack.append(amiga.getContentNode(), dompack.create("span", { className: "overlaytitle", textContent: "Amiga" }));

  let apple = overlaymgr.addRectangle({ left: 45, top: 250, width: 400, height: 50 });
  dompack.append(apple.getContentNode(), dompack.create("span", { className: "overlaytitle", textContent: "Apple" }));

  let atari = overlaymgr.addRectangle({ left: 380, top: 30, width: 80, height: 250 });
  dompack.append(atari.getContentNode(), dompack.create("span", { className: "overlaytitle", textContent: "Atari" }));

  overlaymgr.setSelection([amiga]);

  window.overlaytests = { overlaymgr, amiga, apple, atari }; //allow tests to access us
/*


  let myoverlay1 = new ResizeableOverlay(node, "myoverlays",
        { enabled: true
        , left: 5, top: 50, width: 50, height: 150, selected: true, title: "Amiga", bounds: mybounds
        });

  let myoverlay2 = new ResizeableOverlay(node, "myoverlays",
        { enabled: true
        , left: 45, top: 250, width: 400, height: 50, title: "Apple", bounds: mybounds
        });

  let myoverlay3 = new ResizeableOverlay(node, "myoverlays",
        { enabled: true
        , left: 380, top: 30, width: 80, height: 250, title: "Atari", bounds: mybounds, enabled: false
        });*/
});
